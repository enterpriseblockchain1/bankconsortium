const express = require('express');
const { Gateway, Wallets } = require('fabric-network');
const fs = require('fs');
const path = require('path');
const app = express();
app.use(express.json());

// Static Middleware
app.use(express.static(path.join(__dirname, 'public')));

// Create Account
app.post('/accounts', async (req, res) => {
    try {
        const { id, balance } = req.body;
        const result = await submitTransaction('CreateAccount', id, balance);
        res.status(204).send(result);
    } catch (error) {
        console.error(`Failed to submit transaction: ${error}`);
        res.status(500).send(`Failed to submit transaction: ${error}`);
    }
});

// Query Account
app.get('/accounts/:id', async (req, res) => {
    try {
        const { id } = req.params;
        const result = await evaluateTransaction('QueryAccount', id);
        res.status(200).send(result);
    } catch (error) {
        console.error(`Failed to evaluate transaction: ${error}`);
        res.status(404).send(`Failed to evaluate transaction: ${error}`);
    }
});

// Transfer Funds
app.post('/accounts/transfer', async (req, res) => {
    try {
        const { fromID, toID, amount } = req.body;
        const result = await submitTransaction('TransferFunds', fromID, toID, amount);
        res.status(204).send(result);
    } catch (error) {
        console.error(`Failed to submit transaction: ${error}`);
        res.status(500).send(`Failed to submit transaction: ${error}`);
    }
});

async function getContract() {
    const walletPath = path.join(process.cwd(), 'wallet');
    const wallet = await Wallets.newFileSystemWallet(walletPath);
    const identity = await wallet.get('Admin@bank.bt');
    const gateway = new Gateway();
    const connectionProfile = JSON.parse(fs.readFileSync(path.resolve(__dirname, 'connection.json'), 'utf8'));
    const connectionOptions = { wallet, identity: identity, discovery: { enabled: false, asLocalhost: true } };
    await gateway.connect(connectionProfile, connectionOptions);
    const network = await gateway.getNetwork('bankchannel');
    const contract = network.getContract('bankmgt');
    return contract;
}

async function submitTransaction(functionName, ...args) {
    const contract = await getContract();
    const result = await contract.submitTransaction(functionName, ...args);
    return result.toString();
}

async function evaluateTransaction(functionName, ...args) {
    const contract = await getContract();
    const result = await contract.evaluateTransaction(functionName, ...args);
    return result.toString();
}

app.get('/', (req, res) => {
    res.send('Hello, World!');
});

module.exports = app; // Exporting app
