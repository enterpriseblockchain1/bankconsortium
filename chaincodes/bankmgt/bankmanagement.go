package main

import (
	"encoding/json"
	"fmt"

	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

// Account represents a bank account
type Account struct {
	ID      string `json:"id"`
	Balance int    `json:"balance"`
}

// BankContract provides functions for managing bank accounts
type BankContract struct {
	contractapi.Contract
}

// CreateAccount creates a new account with an initial balance
func (c *BankContract) CreateAccount(ctx contractapi.TransactionContextInterface, id string, balance int) error {
	account := Account{
		ID:      id,
		Balance: balance,
	}

	accountJSON, err := json.Marshal(account)
	if err != nil {
		return err
	}

	return ctx.GetStub().PutState(id, accountJSON)
}

// QueryAccount retrieves the balance of the given account
func (c *BankContract) QueryAccount(ctx contractapi.TransactionContextInterface, id string) (*Account, error) {
	accountJSON, err := ctx.GetStub().GetState(id)
	if err != nil {
		return nil, fmt.Errorf("failed to read from world state: %v", err)
	}
	if accountJSON == nil {
		return nil, fmt.Errorf("the account %s does not exist", id)
	}

	var account Account
	err = json.Unmarshal(accountJSON, &account)
	if err != nil {
		return nil, err
	}

	return &account, nil
}

// TransferFunds transfers funds from one account to another
func (c *BankContract) TransferFunds(ctx contractapi.TransactionContextInterface, fromID string, toID string, amount int) error {
	fromAccount, err := c.QueryAccount(ctx, fromID)
	if err != nil {
		return err
	}

	toAccount, err := c.QueryAccount(ctx, toID)
	if err != nil {
		return err
	}

	if fromAccount.Balance < amount {
		return fmt.Errorf("insufficient funds in account %s", fromID)
	}

	fromAccount.Balance -= amount
	toAccount.Balance += amount

	fromAccountJSON, err := json.Marshal(fromAccount)
	if err != nil {
		return err
	}

	toAccountJSON, err := json.Marshal(toAccount)
	if err != nil {
		return err
	}

	err = ctx.GetStub().PutState(fromID, fromAccountJSON)
	if err != nil {
		return err
	}

	return ctx.GetStub().PutState(toID, toAccountJSON)
}

// Main function
func main() {
	chaincode, err := contractapi.NewChaincode(new(BankContract))
	if err != nil {
		fmt.Printf("Error creating bank contract chaincode: %s", err.Error())
		return
	}

	if err := chaincode.Start(); err != nil {
		fmt.Printf("Error starting bank contract chaincode: %s", err.Error())
	}
}
