# Bank Consortium
This repository contains the code and configuration files for setting up a Hyperledger Fabric network for the Bank Consortium.

## Overview
The Hyperledger Fabric (HLF) banking consortium consists of major financial organizations in Bhutan, including the Bank of Bhutan, Bhutan National Bank, and the Royal Monetary Authority. The consortium uses Hyperledger Fabric, a blockchain platform, to improve the security, efficiency, transparency, and regulatory compliance of financial transactions. Traditional banking processes have inefficiencies, security threats, a lack of transparency, and significant operational expenses. The HLF system features a permissioned network, smart contracts, an immutable ledger, and a modular design. The consortium collaborates with an orderer organization, a banking organization, and the Royal Monetary Authority.

## Features
- Permissioned network
- Smart contracts
- Immutable ledger
- Modular design
- Collaboration with orderer organization, banking organizations, and the Royal Monetary Authority


## Prerequisites
- Docker
- Docker Compose
- Go programming language
- Node.js and NPM




## Network Setup
1. **Generate the Orderer Genesis Block**
   ```bash
   #configtxgen -outputBlock ./orderer/bankgenesis.block -channelID ordererchannel -profile BankOrdererGenesis
   ```
2. **Create the Channel Transaction**
   ```bash
   #configtxgen -outputCreateChannelTx ./bankchannel/bankchannel.tx -channelID bankchannel -profile BankChannel
   ```
3. **Set the Peer Environment**
   ```bash
   #. tool-bins/set_peer_env.sh bank
   ```
4. **Create the Channel**
   ```bash
   #peer channel create -c bankchannel -f ./config/bankchannel/bankchannel.tx --outputBlock ./config/bankchannel/bankchannel.block -o $ORDERER_ADDRESS
   ```
5. **Join the Peer to the Channel**
   ```bash
   #peer channel join -b ./config/bankchannel/bankchannel.block -o $ORDERER_ADDRESS
   ```
6. **List the Channels**
   ```bash
   #peer channel list
   ```


## Chaincode Operations
1. **Approve the Chaincode for the Organization**
   ```bash
   #peer lifecycle chaincode approveformyorg -n certificatemgt -v 1.0 -C nationalschoolchannel --sequence 1 --package-id $CC_PACKAGE_ID
   ```
2. **Check Commit Readiness**
   ```bash
   #peer lifecycle chaincode checkcommitreadiness -n bankmgt -v 1.0 -C bankchannel --sequence 1
   ```
3. **Commit the Chaincode**
   ```bash
   #peer lifecycle chaincode commit -n bankmgt -v 1.0 -C bankchannel --sequence 1
   ```
4. **Query Committed Chaincodes**
   ```bash
   #peer lifecycle chaincode querycommitted -n bankmgt -C bankchannel
   ```
5. **Invoke the Chaincode (Issue Certificate)**
   ```bash
   #peer chaincode invoke -C bankchannel -n bankmgt -c '{"function":"CreateAccount","Args":["1", "1000"]}'
   ```
6. **Query the Chaincode (Read Certificate)**
   ```bash
   #peer chaincode query -C bankchannel -n bankmgt -c '{"function":"QueryAccount","Args":["1"]}'
   ```
7. **Invoke the Chaincode (Delete Certificate)**
   ```bash
   #peer chaincode invoke -C bankchannel -n bankmgt -c '{"function":"CreateAccount","Args":["1", "1000"]}'
   ```

Please note that you may need to replace certain placeholders (e.g., $ORDERER_ADDRESS, $CC_PACKAGE_ID) with the appropriate values for your environment and need to package and install it before chaincode operation.

